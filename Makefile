# For help, do `make help` ( idea from SoftwareCarpentry and victoria.dev )

.PHONY: help build upload 

help: ## Display help
	@echo 'Commands/rules to run:'
	@grep '\s##\s' Makefile | mawk -F':.*?## ' '{printf "    %-28s%s\n", $$1, $$2}'

#build: _build ## Build the HTML `_build` directory

build: 
	jupyter-book build ./

