{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Complexity\n",
    "\n",
    "In these experiments, there are a variety of experimental factors\n",
    "(such as genotypes, samples, unique-molecules) \n",
    "and representations thereof\n",
    "(barcodes, tags, distinguishable variation).\n",
    "The complexity of the representations must exceed the complexity of the factors,\n",
    "and it's best if it greatly exceeds.\n",
    "This is not usually hard to do - if you design things right (ref UMI collisions story).\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Barcodes must uniquely represent a single genotype perturbation, and so must usually be at least as complex (have more potential different variants) than the number of genotypes being represented. Degenerate barcodes are often used, but have the problem that exactly the same barcode could be synthesized multiple times to confound the mapping. Additionally, errors in identifying barcodes can be introduced at many steps during sequencing, and so similar barcodes could be confused as being the same sequence. Pools of distinct barcode oligonucleotides can be programmatically synthesized using DNA chips, but costs can be prohibitive and so many applications use degenerate barcode synthesis of sufficient complexity to avoid confusions. These confusions add noise to measurements by increasing or decreasing the measured frequency of a barcode.\n",
    "    We use the term \"barcode collision\" to refer to this event of multiple experimentally distinct genotypes being confused as one barcode, and apply work from the study of hash collisions (Lamberger et al. 2011) to develop a quantitative analysis of how barcode design can limit these errors. To make collisions improbable, we can make the complexity (number of distinguishable elements) of the barcode library in excess to the complexity of the library of genetic lineages. This can be achieved using longer barcodes with maximal complexity at each position, but each of these parameters comes at a cost. We will survey relevant parameters to best match an appropriate barcode design with the experimental requirements and cost to minimize this source of measurement noise."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The effect of noise on the two paradigms of barcode measurement\n",
    "Barcode sequencing is primarily used in one or two modes of counting to measure the abundance of a sequence of interest. The first is a simple quantitative counting, where the count of an identical barcode sequence is the estimate of that barcode in the sequencing library. Using a proportional (simplex) normalization, median-normalization methods (borrowed from RNAseq tools, (CITE DESeq)), or internal calibrator sequences (IS THERE A CITE FOR THIS?), this is used to calculate the number of barcodes in a sample and thus quantify the genotype variant. The other method is using unique molecular identifiers (UMIs) to achieve quantification through qualitative means (CITE orig 2003). In this approach, a highly degenerate (and therefore likely unique) sequence tag is attached to each individual barcode molecule early in sample processing, thus identifying different input barcode molecules as discrete combinations of barcode and degenerate tag (termed a UMI) in the sequencing output. These are assumed to roughly to the \"one UMI one molecule\" assumption, but this underestimates counts. If the number of molecules counted is large, UMIs collisions must be modeled to ensure accurate quantification (CITE Fu 2011). Diversification of the UMI from errors in sequencing may artificially increase the quantification, and so some analyses use clustering to help avoid this error (umitools). This UMI approach has been extensively used to sequence RNA or amplicon libraries (CITE bulk RNA UMI, lowinput early UMI, CITE NEB PCR pacbio paper, that RNAseq stutter paper) but has not found as much use in barcode sequence despite sharing the similar challenges of quantitative sequence estimation after extensive amplification that UMIs aim to solve (CITE me? anyone else done UMIs and barcodes?).\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Barcode complexity\n",
    "\n",
    "Barcode complexity can be thought of as the number of possible experimental factors that a barcode can\n",
    "possibly represent.\n",
    "\n",
    "This is often genotypes in a pooled genetics assay, or different samples with a sample-indexed sequencing\n",
    "barcode.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Collisions\n",
    "\n",
    "Complexity of a barcode design and a barcode library\n",
    "\n",
    "The most basic design of a barcode is degenerate nucleotides, so NNNN represents 4^4 different theoretical barcodes. This is termed the complexity of a barcode. A library of barcodes, a pool of them, has a number of elements within it, so that's the complexity. One's got to be smaller than the other. How much smaller? Degenerate barcode synthesis uses a mixture of nucleotides at certain defined positions to generate a library of different oligonucleotide sequences with complexity exponential to the number of degenerate positions. This is a cheap and simple method to obtain a complex mixture of sequences, but there is a risk of exact barcode collisions by  independent synthesis. This chance is reduced by extending the barcode, so that with $n$ degenerate positions the possible size of the library grows to as much as $4^n$. Therefore, the chance that two particular barcodes are identical, despite being synthesized independently, is as low as $\\frac{1}{4^n}$. However, the practical limitations of sequence length constrain the total length of the construct as well as the structure of degeneracy (discussed later in the context of library synthesis).\n",
    "\n",
    "If we consider the task of barcoding a library of $10^4$ genetic lineages, then seven nucleotides seems to provide more than enough ($ 4^7 = 16,384 > 10,000 $) barcodes with which to theoretically barcode the genotypes, but this is not actually the case. The chance of any particular two being identical is $4^-7$, but there are $\\choose{10^4}{2}$ possible combinations of individual barcodes. Multiplying these two to calculate an expected number of identical combinations ($\\lambda = {10,000 \\choose 2}\\frac{1}{4^7}$) we can use an poisson distribution to estimate the chance that at least one collision occurs as $1 - \\frac{\\lambda^0 e^{-\\lambda}}{k!} = 1 - e^{- \\frac{ {10,000 \\choose 2} }{4^7}} \\approx 1$, and so it is almost certain that at least two barcodes are identical. The phenomenon is well known as the birthday problem, paradox, or attack (depending on the application), and the 50% chance of collision tends to occur upon drawing $\\sim 1.2 \\sqrt{s}$ barcodes from a set of unique barcodes of size $s$ (CITE that 60's paper?). For a library of length $n=7$ DNA barcodes, there is a $\\sim 50.4\\%$ chance that at least one collision occurs in $ b = 152 $ barcodes ( $1.2 \\sqrt{4^7} = 153.6$ , $ 1 - e^{ - \\frac{ {152 \\choose 2} }{ 4^7} } = 50.4\\% $ ). For a library of size 10,000 barcodes, it would take a barcode of length 14 before the probability of collision drops below 50%, without accounting for any tolerance to errors in sequencing. The same quantitative principle applies when designing a UMI to assist in quantification, where the complexity should be appropriate for the expected quantification (although it is more robust to collisions (CITE fu 2011)). Thus we see that lengthening the barcode to increase library complexity allows more \"barcode space\", possible different barcodes, and makes random collisions by synthesis less likely.\n",
    "An easy heuristic is to design the barcode library to be much larger than (variant1.2)2. Here's why: A barcode collision is a term we're borrowing from cryptography (aka hash collisions) that denotes when two barcodes resemble each other without representing the same lineage. The can be easily thought of as two randomly generated barcodes being exactly the same, like drawing ATC and ATC from a pool of 4^3 = 64 barcodes. The birthday problem offers a hueristic to approximate when this becomes likely: \n",
    "number of barcodes >= 1.2 * sqrt( size of barcode space ).\n",
    "\n",
    "This is less of a problem if you are synthesizing chosen barcodes, but who is these days?\n",
    "\n",
    "Adjusting for biased complexity in degenerate synthesis\n",
    "A degenerate base N is a synthesis design is usually supposed to indicate a 25% chance of any letter, but biochemical biases exist in synthesis and barcode integration that can adjust the nucleotide frequencies to something other than 25%. This will constrain the complexity of the barcodes such that they are more likely to collide than would be expected by chance, but sometimes this can be used to obtain or avoid properties of the barcode loci such as restriction digest sites or GC bias. We model the degree of constraint of degeneracy on a barcode by comparing the expected information content of a barcode given different degrees of nucleotide diversity. We can calculate the bits of information in a particular mix of bases, estimated by the shannon entropy $H = \\sum_i -p_i log_2{p_i} $ , for each $p_i$ proportion of a particular nucleotide at that position. For example, with an ideal mix of 25% each nucleotide contributes 2 bits to the barcode's information, while an extremely biased mix of 0% G and 0% C would contribute only 1 bit per position. This less complex barcode would require twice the length to provide sufficient complexity, but by calculating this for each base (for example 1.6 bits for IUPAC code V) you can adjust the barcode length to provide the same informative complexity to span your barcode library design.\n",
    "\n",
    "The effects of errors on barcode collisions\n",
    "\n",
    "A barcode can be detected with, or accumulate, sufficient errors to be counted as another related barcode sequence. Error-mediated barcode collisions is one big factor confounding quantification of barcoded lineages. If a barcode is mis-identified as another with some low rate, this will interact with per-barcode abundances where an abundant barcode may be more commonly mistaken as a low abundance barcode than one might naively expect. Imbalanced libraries, by design or by selection, mean there's an abundance-dependence of the effect of errors propagating into other barcodes. The likelihood of this collision by error depends on the edit distance between the two barcodes and the abundance of these barcodes. An edit distance is the distance between two sequences expressed in terms of the edit operations it would take to change one sequence into the other. There are multiple types of possible edits, but for simplicity we assume all substitutions to be of equal likelihood and thus symmetrical. There are different kinds of edit distances. Hamming edit distance only counts the minimum number of substitutions it would take to convert one sequence into the other, but the more biologically useful Levenshtein distance also counts the possibility of editing by single insertions or deletions. This is important to consider for the analysis of barcode sequences, especially as a single indel early in a read can cause a shift in the whole barcode sequence that is more accurately quantified by Levenshtein distance than Hamming distance. While the Levenshtein distance (or variants thereof citeFREEbarcodes) should be used for the purposes of bioinformatic analysis, Illumina technologies have approximately 100-fold higher rates of per-base substitutions than indels (Schirmer et al. 2016) and so principles from work with Hamming distances should still be quite relevant. More exotic error modes may suggest the use of more complex edit distances but the rarity discourages consideration.\n",
    "\n",
    "A library designed with barcodes with a distance of one edit/error between barcodes is strongly discouraged, as a single mutation may result in the reduction of one barcode's count and the increase of another's. Designing a library of barcodes that are at least three edits away from any other barcode can tolerate one single mistake and still correct that measured barcode sequence back to the nearest correct barcode. More edits offer more protection from rare noise events, but for our work with libraries of thousands of genotypes each represented by multiple replicate barcodes, mutations become common place and we use (and strongly recommend) a minimum distance of four errors, or edits, away from any other barcode. As library sizes will likely expand, here we expand upon a quantitative accounting of this to help guide future design choices.\n",
    "\n",
    "To do this, we can extend the \"birthday attack\" analysis by considering a \"near-collision\" as a barcode being within some distance $r$ of another barcode. A \"near-collision\" is unacceptable because the barcodes may close enough to be confused by mutation or analysis. We can calculate the number of $n$ length barcodes that are $r$ or fewer edits away from a barcode as $\\sum_{i=0}^{r} {n \\choose i} $ (Lamberger et al. 2011). Setting $r$ to some number, say the minimum edit distance of 4, we can calculate the space occupied by a single barcode. For barcodes of length $n=14$, this means a single barcode in this analysis occupies the 1471 barcodes within 4 edits of the barcode. Thus the probability that any random barcode collides with this set is $ 1471 \\times 4^{-14} $, then the expected number of collisions with $ b $ barcodes is $ {b \\choose 2} \\times 1471 \\times 4^{-14} $, and the expected number of barcodes $b$ before this near-collision occurs is $\\approx 1.2 \\frac{\\sqrt{ 4^{14} } }{ \\sqrt{1471} } $. For length $n=14$ barcodes this is a mere $\\sim 513$ barcodes, but for length $n=20$ barcodes this is $\\sim15985$. Thus a length $n=20$ barcode (as we routinely use) allows for at least 10,000 barcodes before it becomes likely that at least two barcodes come within 4 edits of each other. Designing barcodes with more degenerate bases allows for more unique barcodes but also more minimum edit distance to bioinformatically correct for any errors that may be produced during library preparation and sequencing.\n",
    "\n",
    "Abundance interacts with error-rates to bias noise\n",
    "\n",
    "The amount of noise contributed from various collisions depends on the abundance of genotype variants in the library. If a barcode is more abundant, then any single random event occurring to that barcode is less impactful, but any constant error rate will more likely originate from an abundant barcode. If there is only a ~0.1% per-base error rate (illumina whatever seq kircher), then if a six base-pair barcode has a real abundance of 1,000 barcodes we would expect there to be about six reads with an edit of distance one from that particular barcode. Across millions of reads for the whole sample, we would expect to have many of such errors, and also errors resulting from multiple edits. These mismatches may not largely impact the estimation of the original barcode, as they are rare and should scale linearly with abundance, the real problem arises when considering the effect on a neighboring barcode. An amount of errant reads low relative to its parent barcode may be large relative to another more lowly abundant barcode. For programmed barcode synthesis technologies, this principle may be useful in suggesting that very abundant genotypes (like wild-type for normalization purposes) should use barcodes that are exceptionally distinct from other variants to avoid rare collisions with other barcodes. For degenerate synthesis, abundant normalization strains may be synthesized independently as part of a panel of calibrator strains with distinct barcodes to be pooled in at the time of culturing.\n",
    "\n",
    "Libraries of uniform abundance are less vulnerable to noise, but it is exactly the enrichment and biasing of the distribution that is used to estimate the phenotype of interest. This can become a particular issue when some strains are so abundant as to crowd out less abundant strains (cite FITseq), or when largely abundant barcodes have a larger total amount of \"pollution\" (find different word) of barcodes neighboring in sequence graphs (CITE look in UMI tools, bartender, something to find the right term and citation). The event of one barcode being mistaken as another can be rare (let's assume 1 in 100 chance for now) for the lowly abundant barcode (10 counts, expect 0.1 mistakes) but not for a more abundant barcode (1000 counts, expect 10 mistakes). This has a fixed proportion effect on each original barcode (mean is 99% of original sequence, but with higher variance), but if the barcode is mistakenly identified as a neighbor in sequence space (ie a similar barcode sequence) then it's effect can be much larger on the lowly abundant variant (10% error) as compared to its effect on the highly abundant variant (0.1% error). For quantification via UMI labelling this approach has similar drawbacks in the lower ranges, where each distinct UMI is very likely a distinct original molecule, but as label space saturates both the effect of a mutation and the informative value of an observation have a decreasing effect (CITE Fu2011). Thus, to minimize the effect of noise of measurement of all barcodes, we must consider that the effect may be more pronounced on the lowly abundant variants in libraries of biased distribution or in complex libraries with many barcodes.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Nucleotide complexity\n",
    "\n",
    "Complexity of nucleotides refers to the variability of nucleotide identity at a particular position.\n",
    "While the total nucleotide complexity is most important for high barcode complexity, per-nucleotide diversity\n",
    "is an important technical consideration for sequencing.\n",
    "\n",
    "Illumina sequencers crap out if you flash teh same color at them. I mean, who can blame them, if they only\n",
    "see one color at a time they think things are broken. So, you've got to make sure your bonafide good seq\n",
    "doesn't flash all red or green or nothing at the same time. Poor lil sequencer.\n",
    "\n",
    "Couple strategies to deal with this:\n",
    "\n",
    "### Phased barcodes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Complexity of factors\n",
    "\n",
    "## numbers of strains\n",
    "\n",
    "Hey... estiamte your strains my dude\n",
    "\n",
    "Refer back to collisions\n",
    "\n",
    "## replicates per strains\n",
    "\n",
    "Why barcodes are better readouts than direct read variation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Redundant replicates are robust to collisions, noise\n",
    "The mapping between genotype and barcode need not be one-to-one, but can be a mapping with multiple barcodes for one genotype (but never vice versa). If multiple members of a clonal lineage are barcoded several times with different barcodes, then each of the barcodes is actually representing a replicate of generating that barcoded genotype. At the cost of increasing the number of barcodes in the library, this can serve multiple purposes in the experiment. Replicates can be used to better estimate the mean abundance of a barcode, estimate its variance across time, and provide a way to detect unusual distributions of genotype fitnesses (CITE JAFFE2017). Barcode replicates can also serve a technical role as back-ups in case two degenerately synthesized barcodes collide, either perfectly or within an edit distance that is unacceptable for the design. Since a collision is a rare event, it becomes more rare that multiple replicate barcodes collide. Thus, replicates can counter-intuitively allow for reliable use of a certain barcode design by increasing the complexity in a way that increases error-tolerance.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
